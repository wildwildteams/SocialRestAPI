const express = require('express');
const bodyParser = require('body-parser');
const logger = require('morgan');
const path = require('path');
const router = require('./routes');
const { allowOrigin } = require('./utils');
const app = express();
const PORT = 8088;

app.use(logger('dev'));
app.disable('etag');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/static', express.static(path.join(__dirname, 'public')));

app.use(allowOrigin);
app.use('/api/v1', router);

app.listen(PORT, () => {
  console.log(`We are live on ${PORT}`);
});
