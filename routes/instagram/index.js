const instagram = require('instagram-node').instagram();
const { setAccessToken, handleGetData, handleInstaTokenGet } = require('./utils');

exports.get = (req, res) => {
  if (!req.query.id) res.status(400).json({ error: 'Not enough information to run the query' });
  handleInstaTokenGet(req.query.service_token)
    .then((token) => setAccessToken(instagram, token))
    .then((instagram) => handleGetData(instagram, req))
    .then((result) => res.status(200).json(result))
    .catch((error) => res.status(error.code).json({ error: error.message }));
};

exports.post = (req, res) => res.status(501);
exports.put = (req, res) => res.status(501);
exports.delete = (req, res) => res.status(501);
