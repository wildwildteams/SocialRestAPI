const UserModel = require('../../database/models/user');
const InstagramPostModel = require('../../database/models/instagramPost');
const InstagramUserModel = require('../../database/models/instagramUser');
const moment = require('moment');

exports.handleInstaTokenGet = (serviceToken) => new Promise((resolve, reject) => {
  const user = UserModel.findOne({ service_token: serviceToken }).exec();
  user.then((result) => {
    if ((result) && (result.insta_token)) {
      resolve(result.insta_token);
    }
    else {
      reject({ code: 404, message: 'Token not found' });
    }
  })
    .catch(() => {
      reject({ code: 501, message: 'Not Implemented' });
    });
});

exports.setAccessToken = (instagram, access_token) => {
  instagram.use({ access_token });
  return instagram;
}

exports.handleGetData = (instagram, req) => {
  switch (req.params.dataType) {
    case 'profile':
      return handleUser(instagram, req.query.id)
        .then((result) => {
          writeProfileHistory(req.query.id, result).catch((err) => console.log(err))
          return result;
        })
        .catch((err) => { throw Object({ ...err, code: 500 }); });

    case 'post':
      return handleMediaQuery(instagram, req.query.id)
        .then((result) => {
          writePostHistory(req.query.id, result).catch((err) => console.log(err))
          return result;
        })
        .catch((err) => { throw Object({ ...err, code: 500 }); });

    default:
      throw Object({ message: 'SOMETHING HAPPENED IN handleGetData', code: 500 });
      break;
  }
}

const handleMediaQuery = (instagram, id) => new Promise((resolve, reject) =>  
  instagram.media_shortcode(id, (error, result) => (error
    ? reject(error)
    : resolve({
      likes: result.likes.count,
      comments: result.comments.count,
      caption: result.caption.text,
    })
  ))
);

const handleUser = (instagram, id) =>  new Promise((resolve, reject) =>  
  instagram.user(id, (error, result) => (error
    ? reject(error)
    : resolve({
      username: result.username,
      media_count: result.counts.media,
      followers: result.counts.followed_by,
      followed: result.counts.follows,
    })
  ))
);

const writeProfileHistory = (id, result) => {
  const InstagramUser = new InstagramUserModel({
    id,
    username: result.username,
    media_count: result.media_count,
    followers: result.followers,
    followed: result.followed,
  });
  return historyTimeIsSpend(id, 'profile')
    .then(shouldWrite => shouldWrite && InstagramUser.save()
    .catch((err) => {
      throw Object({ ...err, code: 500 });
    })
  );
};

const writePostHistory = (item, result) => {
  const InstagramPost = new InstagramPostModel({
    id: item,
    likes: result.likes,
    comments: result.comments,
    caption: result.caption,
  });
  return historyTimeIsSpend(item, 'post')
    .then(shouldWrite => shouldWrite && InstagramPost.save()
    .catch((err) => {
      throw Object({ ...err, code: 500 });
    })
  );
};

const getSeconds = (createdAt) => createdAt ? moment.duration(moment().diff(moment(createdAt))).asSeconds() : -1;

const shouldWrite = (createdAt) => getSeconds(createdAt) < 0 || getSeconds(createdAt) > 60;

const historyTimeIsSpend = (id, type) => {
  if (type === 'post') {
    return InstagramPostModel.findOne({ id: id }).sort({ created_at: -1 })
      .then(result => result && result.created_at)
      .then(created_at => shouldWrite(created_at))
      .catch((err) => console.log(err))
  } else if (type === 'profile') {
    return InstagramUserModel.findOne({ id: id }).sort({ created_at: -1 })
      .then(result => result && result.created_at)
      .then(created_at => shouldWrite(created_at))
      .catch((err) => console.log(err))
  }
}
