const Xray = require('x-ray');
const TripAdvisorPageModel = require('../../database/models/tripAdvisorPage');
const moment = require('moment');


exports.getData = (link) => new Promise((resolve, reject) => {
  if (!checkUrl(link)) {
    reject({ error: 'Invalid url', code: 400 });
  }
  const x = Xray();
  x(link, '#PAGE', {
    name: 'h1.heading_title',
    rating: '.header_rating span.ui_bubble_rating@content',
    images_сount: 'div.carousel_images span.see_all_count',
    images: x('div.carousel_images', ['span.imgWrap noscript img@src']),
    reviews_count: 'a.more span',
    reviews: x('#taplc_location_reviews_list_0', ['div.review']),
  })((err, obj) => {
    if (err) {
      reject({
        ...err,
        code: 500,
      });
    }
    resolve(obj);
  });
});

exports.formatData = (result) => {
  if (result) {
    const copy = Object.assign({}, result);
    copy.images_сount = Number.parseInt(copy.images_сount ?
      copy.images_сount.replace(/[^\d.]/ig, '') :
      null, 10);
    copy.rating = Number.parseFloat(copy.rating ?
      copy.rating.replace(',', '.') :
      null);
    copy.reviews_count = Number.parseFloat(copy.reviews_count ?
      copy.reviews_count :
      null);
    return copy;
  }
};

exports.writeHistoryToDB = (siteLink, result) => {
  const tripAdvisorPage = new TripAdvisorPageModel({
    link: siteLink,
    rating: result.rating,
    reviews_count: result.reviews_count,
  });

  return historyTimeIsSpend(siteLink)
    .then(shouldWrite => shouldWrite && tripAdvisorPage.save()
      .catch((err) => {
        throw Object({ ...err, code: 500 });
      })
    );
};

const getSeconds = (createdAt) => createdAt ? moment.duration(moment().diff(moment(createdAt))).asSeconds() : -1;

const shouldWrite = (createdAt) => getSeconds(createdAt) < 0 || getSeconds(createdAt) > 60;

const historyTimeIsSpend = (link) => TripAdvisorPageModel.findOne({ link: link }).sort({ created_at: -1 })
  .then(result => result && result.created_at)
  .then(created_at => shouldWrite(created_at));

const tripadvisorRegex = /(\.|\/\/)tripadvisor\./g;
const checkUrl = (link) => link.search(tripadvisorRegex) !== -1;
