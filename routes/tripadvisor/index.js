const { getData, formatData, writeHistoryToDB } = require('./utils');

exports.get = (req, res) => {
  if (!req.query.link) res.status(400).json({ error: 'Not enough information to run the query' });
  if (req.params.dataType === 'profile') {
    getData(req.query.link)
      .then(result => formatData(result))
      .then(result => {
        writeHistoryToDB(req.query.link, result).catch((err) => console.log(err))
        res.status(200).json({ result })
      })
      .catch(error => res.status(error.code).json({ ...error }));
  }
};


exports.post = (req, res) => res.status(501);
exports.put = (req, res) => res.status(501);
exports.delete = (req, res) => res.status(501);
