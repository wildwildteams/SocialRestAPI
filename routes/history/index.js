const { getTripAdvisorHistory, instagramPostHistory, instagramUserHistory } = require('./utils');

exports.get = (req, res) => {
  let limit = 20;
  let skip = 0;
  if ((req.query.limit) && (req.query.limit > 0) && (req.query.limit < 1000)) limit = parseInt(req.query.limit);
  if ((req.query.skip) && (req.query.skip > 0)) skip = parseInt(req.query.skip);
  if (req.params.driver === 'instagram') {
    if (req.query.type === 'post') {
      const history = instagramPostHistory(req.query.id, skip, limit);
      history.then(result => res.status(200).json(result)).catch(error => res.status(500).json(error));
    }
    if (req.query.type === 'profile') {
      const history = instagramUserHistory(req.query.id, skip, limit);
      history.catch(error => res.status(500).json(error)).then(result => res.status(200).json(result));
    }
  }
  else if (req.params.driver === 'tripadvisor') {
    const history = getTripAdvisorHistory(req.query.link, skip, limit);
    history.then(result => res.status(200).json(result)).catch(error => res.status(500).json(error));
  }
};


exports.post = (req, res) => res.status(501);
exports.put = (req, res) => res.status(501);
exports.delete = (req, res) => res.status(501);
