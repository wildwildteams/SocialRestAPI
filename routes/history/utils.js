const InstagramPostModel = require('../../database/models/instagramPost');
const InstagramUserModel = require('../../database/models/instagramUser');
const TripAdvisorPageModel = require('../../database/models/tripAdvisorPage');

exports.getTripAdvisorHistory = (objectLink, skip, limit) => TripAdvisorPageModel.find({ link: objectLink }).limit(limit).skip(skip).sort({ created_at: -1 });
exports.instagramUserHistory = (objectLink, skip, limit) => InstagramUserModel.find({ id: objectLink }).limit(limit).skip(skip).sort({ created_at: -1 });
exports.instagramPostHistory = (objectLink, skip, limit) => InstagramPostModel.find({ id: objectLink }).limit(limit).skip(skip).sort({ created_at: -1 });
