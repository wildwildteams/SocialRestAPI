const crypto = require('crypto');

exports.tokenGenarate = (email) => crypto.createHash('sha1').update(email).digest('hex');
exports.hadnleError = (error) => {
  let result = {
    responseStatus: 501,
    responseText: '',
  };
  switch (error.code) {
    case 11000:
      result = { responseStatus: 406, responseText: 'Email or token is allready used!' };
      break;
    default:
      result = { responseStatus: 501, responseText: 'Unexpected error' };
      break;
  }
  return result;
};

exports.getAccountUpdate = (req) => {
  const result = {};
  if (req.body.email) result.email = req.body.email.trim();
  if (req.body.insta_token) result.insta_token = req.body.insta_token.trim();
  return result;
};
