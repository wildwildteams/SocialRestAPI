const { tokenGenarate, hadnleError, getAccountUpdate } = require('./utils');
const UserModel = require('../../database/models/user');

exports.post = (req, res) => {
  if ((!req.body.email) || (!req.body.insta_token)) res.status(400).json({ error: 'Not enough information to run the query' });
  const user = new UserModel({
    service_token: tokenGenarate(req.body.email),
    email: req.body.email,
    insta_token: req.body.insta_token,
  });
  user.save()
    .then((result) => {
      res.status(201).json({
        status: 'ok',
        service_token: result.service_token,
      });
    })
    .catch((error) => {
      const cleanError = hadnleError(error);
      res.status(cleanError.responseStatus).json({
        status: 'error',
        error_text: cleanError.responseText,
      });
    });
};

exports.get = (req, res) => {
  const user = UserModel.findOne({ service_token: req.query.service_token }).exec();
  user.then((result) => {
    if (result) {
      res.status(200).json(result);
    }
    else {
      res.status(404).json({
        status: 'error',
        error_text: 'User not found',
      });
    }
  })
    .catch((error) => {
      const cleanError = hadnleError(error);
      res.status(cleanError.responseStatus).json({
        status: 'error',
        error_text: cleanError.responseText,
      });
    });
};

exports.put = (req, res) => {
  const dataToUpdate = getAccountUpdate(req);
  UserModel.findOneAndUpdate(
    { service_token: req.body.service_token },
    { $set: dataToUpdate },
  )
    .then((result) => {
      if (result) {
        res.status(200).json(result);
      }
      else {
        res.status(404).json({
          status: 'error',
          error_text: 'User not found',
        });
      }
    })
    .catch((error) => {
      const cleanError = hadnleError(error);
      res.status(cleanError.responseStatus).json({
        status: 'error',
        error_text: cleanError.responseText,
      });
    });
};

exports.delete = (req, res) => {
  UserModel.findOneAndRemove({ service_token: req.body.service_token })
    .then((result) => {
      if (result) {
        res.status(200).json(result);
      }
      else {
        res.status(404).json({
          status: 'error',
          error_text: 'User not found',
        });
      }
    })
    .catch((error) => {
      const cleanError = hadnleError(error);
      res.status(cleanError.responseStatus).json({
        status: 'error',
        error_text: cleanError.responseText,
      });
    });
};
