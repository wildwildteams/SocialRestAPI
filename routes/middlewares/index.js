const userModel = require('../../database/models/user');


exports.serivceTokenMiddleware = (req, res, next) => {
  const { method, path, body, query } = req;
  if ((method === 'POST') && (path === '/account')) {
    next();
    return;
  }
  const serviceToken = method === 'GET'
    ? query.service_token
    : body.service_token;
  const user = userModel.findOne({ service_token: serviceToken }).exec();
  user.then((result) => {
    if (result) {
      next();
      return;
    }
    res.status(401).json({
      status: 'error',
      error_text: 'User Unauthorized',
    });
  }).catch((error) => {
    res.status(501).json({
      status: 'error',
      error_text: error.code,
    });
  });
};
