const router = require('express')();
const account = require('./account');
const tripadvisor = require('./tripadvisor');
const instagram = require('./instagram');
const history = require('./history');
const { serivceTokenMiddleware } = require('./middlewares');

router.use(serivceTokenMiddleware);

router.route('/account')
  .post(account.post)
  .get(account.get)
  .put(account.put)
  .delete(account.delete);

router.route('/data/tripadvisor/:dataType/')
  .post(tripadvisor.post)
  .get(tripadvisor.get)
  .put(tripadvisor.put)
  .delete(tripadvisor.delete);

router.route('/data/instagram/:dataType/')
  .post(instagram.post)
  .get(instagram.get)
  .put(instagram.put)
  .delete(instagram.delete);

router.route('/data/history/:driver/')
  .post(history.post)
  .get(history.get)
  .put(history.put)
  .delete(history.delete);


module.exports = router;
