const mongoose = require('../mongoose');

const instagramPostSchema = mongoose.Schema({
  id: {
    type: mongoose.Schema.Types.String,
    required: true,
  },
  likes: {
    type: mongoose.Schema.Types.Number,
    required: true,
  },
  comments: {
    type: mongoose.Schema.Types.Number,
    required: true,
  },
  caption: {
    type: mongoose.Schema.Types.String,
    required: true,
  },
}, { versionKey: false, timestamps: { createdAt: 'created_at' } });

module.exports = mongoose.model('InstagramPost', instagramPostSchema);
