const mongoose = require('../mongoose');

const tripAdvisorPageSchema = mongoose.Schema({
  link: {
    type: mongoose.Schema.Types.String,
    required: true,
  },
  rating: {
    type: mongoose.Schema.Types.Number,
    required: true,
  },
  reviews_count: {
    type: mongoose.Schema.Types.Number,
    required: true,
  },
}, { versionKey: false, timestamps: { createdAt: 'created_at' } });

module.exports = mongoose.model('TripAdvisorPage', tripAdvisorPageSchema);
