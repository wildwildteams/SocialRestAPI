
const mongoose = require('../mongoose');

const userSchema = mongoose.Schema({
  email: {
    type: mongoose.Schema.Types.String,
    unique: true,
    required: true,
  },
  insta_token: {
    type: mongoose.Schema.Types.String,
    required: true,
  },
  service_token: {
    type: mongoose.Schema.Types.String,
    required: true,
  },
}, { versionKey: false });

module.exports = mongoose.model('User', userSchema);
