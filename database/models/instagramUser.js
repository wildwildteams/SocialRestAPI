const mongoose = require('../mongoose');

const instagramUserSchema = mongoose.Schema({
  id: {
    type: mongoose.Schema.Types.String,
    required: true,
  },
  username: {
    type: mongoose.Schema.Types.String,
    required: true,
  },
  media_count: {
    type: mongoose.Schema.Types.Number,
    required: true,
  },
  followers: {
    type: mongoose.Schema.Types.Number,
    required: true,
  },
  followed: {
    type: mongoose.Schema.Types.Number,
    required: true,
  },
}, { versionKey: false, timestamps: { createdAt: 'created_at' } });

module.exports = mongoose.model('InstagramUser', instagramUserSchema);
