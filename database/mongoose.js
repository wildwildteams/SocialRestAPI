const Mongoose = require('mongoose');
const { config } = require('../config');

Mongoose.Promise = global.Promise;

const { User, Password, Host, Port, Name } = config.db;
Mongoose.connect(`mongodb://${User}:${Password}@${Host}:${Port}/${Name}`, { config: { autoIndex: false } });

module.exports = Mongoose;

