exports.allowOrigin = (req, res, next) => {
  const API_URL = process.env.API_URL || '127.0.0.1';
  const PORT = process.env.PORT || 8080;
  res.setHeader('Access-Control-Allow-Origin', `http://${API_URL}:${PORT}`);
  next();
};

